# Challenge Project 3 Pendataan Bansos (Frontend JS Project) - From Jabar Digital Service

## Informasi Peserta

Faris Hadi Abdullah
Kelas Front-End : Vue JS
Link Website : https://form-bansos-faris.netlify.app
Video Demo : https://youtu.be/YO1uCa7A78U

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
