import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: { 
    options: {
        customProperties: true,
    },
    themes: {
      light: {
        primary: '#1E88E5',
        secondary: '#FFD026',
        accent: '#16A75C',
      },
      dark: true
    },
  },
    icons: {
        iconfont: 'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
      },
      
});