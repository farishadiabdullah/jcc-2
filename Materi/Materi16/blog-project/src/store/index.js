import Vue from 'vue'
import Vuex from 'vuex'
import counter from "./counter"
import alert from './alert'
import dialog from './dialog'
import auth from './auth'
import VuexPersist from 'vuex-persist'

// Create a new store instance
const vuexPersist = new VuexPersist({
    key: 'sanbercode',
    storage: localStorage
});

Vue.use(Vuex)

export default new Vuex.Store({
    // state: {
    //     count:0
    // },
    // getters: {
    //     count: (state) => {
    //         return state.count
    //     }
    // },
    // mutations: {
    //     increment: (state, payload) => {
    //         state.count += payload
    //     }
    // },
    // actions: {

    // }
    // pindah ke counter.js
    plugins: [vuexPersist.plugin],
    modules: {
        counter,
        alert,
        dialog,
        auth
    }
})