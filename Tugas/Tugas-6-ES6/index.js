// soal 1

const luaspp = (panjang, lebar) => {
    return `${panjang * lebar}`;
 }
 
 const kelilingpp = (panjang, lebar) => {
    return `${(2 * panjang) + (2 * lebar)}`;
 }
 
 let panjang = 5
 let lebar = 3
 
 console.log(kelilingpp(panjang,lebar),luaspp(panjang,lebar));

// soal 2

const newFunction = (firstName, lastName) => {
    return {
      firstName : firstName,
      lastName : lastName,
      fullName: function () {
        console.log(`${firstName} ${lastName}`)
      }
    } 
  }
  
//Driver Code 
newFunction("William", "Imoh").fullName()

// soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
  
const {firstName, lastName, address, hobby} = newObject

//Driver Code
console.log(firstName, lastName, address, hobby);

// Soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]

//Driver Code
console.log(combined);

// soal 5

const planet = "earth" 
const view = "glass"

const gabungan = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`

console.log(gabungan);