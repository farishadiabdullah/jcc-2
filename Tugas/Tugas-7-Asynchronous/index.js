// soal 1

var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// function tampil(time, books, a) {
//     if (a < books.length) {
//         readBooks (time, books[a], function(sisa) {
//             if (sisa > 0) {
//                 a += 1;
//                 tampil(sisa, books, a)
//             }
//         })
//     }
// }

// tampil(10000, books, 0);

// jawaban soal 1

const time = 10000

readBooks (time , books[0] , (sisaWaktu0) => {
    readBooks (sisaWaktu0 , books[1] , (sisaWaktu1) => {
        readBooks (sisaWaktu1 , books[2] , (sisaWaktu2) => {
            readBooks (sisaWaktu2 , books[3] , (sisaWaktu3) => {
                console.log(sisaWaktu3)
            })
        })
    })
})

// done selesai soal 1