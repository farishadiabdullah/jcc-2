// Soal 1
var kalimat_1 = "  Halo     nama saya Muhammad Iqbal Mubarok  "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(kalimat){
  var str = kalimat.split(' ')
  
  var newStr = str.filter( function(el){
    return el != ''
  })

  return newStr.length 
}

// console.log(jumlah_kata(kalimat_3))

//soal 2

/*
  1. bukan di akhir bulan => tanggal + 1 , bulan tetap , tahun tetap 
  2. di akhir bulan : 
     2.1 bukan bulan 12 : tanggal 1 , bulan + 1 , tahun tetap 
     2.2 bulan 12 : tanggal 1 , bulan 1 , tahun + 1
*/

/*
  1. tahun habis dibagi 4
     2.1 satu abad (2000 , 1900 , 2300, 3100) 
          2.1.1 habis dibagi 400 => kabisat => akhir bulan 29
          2.1.1 jika tidak habis dibagi 400 => bukan kabisat => akhir bulan 28
    2.2 bukan satu abad (2012 , 2008) => kabisat => 29


  2. tahun tidak habis dibagi 4 => bukan kabisat => akhir bulan 28
*/

function next_date( hari , bulan , tahun){

  var akhir_bulan_2
  if(bulan == 2){
    if(tahun % 4 == 0){
      
      if(tahun % 100 == 0){

        if(tahun % 400 == 0){
          akhir_bulan_2 = 29
        }else{
          akhir_bulan_2 = 28
        }

      }else{
        akhir_bulan_2 = 29
      }

    }else{
      akhir_bulan_2 = 28
    }
  }

  var tanggal_akhir_bulan = {
    1 : 31,
    2 : akhir_bulan_2,
    3 : 31,
    4 : 30,
    5 : 31,
    6 : 30,
    7 : 31,
    8 : 31,
    9 : 30,
    10 : 31,
    11 : 30,
    12 : 31
  }

  if( hari != tanggal_akhir_bulan[bulan]){
    hari = hari + 1
  }else{
    if(bulan != 12){
      hari = 1
      bulan = bulan + 1
    }else{
      hari = 1
      bulan = 1
      tahun = tahun + 1
    }
  }

  var bulan_str = [
    'Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober',
    'November' , 'Desember' 
  ]

  return hari + " " + bulan_str[bulan - 1] + " " + tahun

}

var tanggal = 29
var bulan = 2
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun )) // output 1 Maret 202