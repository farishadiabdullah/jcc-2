/// ARRAY
var hobbies = ["coding", "cycling", "climbing", "skateboarding"] 
console.log(hobbies) // [ 'coding', 'cycling', 'climbing', 'skateboarding' ]
console.log(hobbies.length) // 4 
 
console.log(hobbies[0]) // coding
console.log(hobbies[2]) // climbing
// Mengakses elemen terakhir dari array
console.log(hobbies[hobbies.length -1]) // skateboarding

var feeling = ["dag", "dig"]
feeling.push("dug") // menambahkan nilai "dug" ke index paling belakang
console.log(feeling) // ["dag", "dig", "dug"]
feeling.pop() // menghapus nilai "dug" dari index paling belakang
console.log(feeling) // ["dag", "dig"]

//push
var numbers = [0, 1, 2]
numbers.push(3)
console.log(numbers) // [0, 1, 2, 3]
// Bisa juga memasukkan lebih dari satu nilai menggunakan metode push
numbers.push(4, 5)
console.log(numbers) // [0, 1, 2, 3, 4, 5] 

//pop
var numbers = [0, 1, 2, 3, 4, 5]
numbers.pop() 
console.log(numbers) // [0, 1, 2, 3, 4]

//unshift
var numbers = [0, 1, 2, 3]
numbers.unshift(-1) 
console.log(numbers) // [-1, 0, 1, 2, 3]

//shift
var numbers = [ 0, 1, 2, 3]
numbers.shift()
console.log(numbers) // [1, 2, 3] 

//sort
var animals = ["kera", "gajah", "musang"] 
animals.sort()
console.log(animals) // ["gajah", "kera", "musang"]

//coba sort
var animals = ["3. kera", "2. gajah", "1. musang"] 
animals.sort()
console.log(animals) // ["gajah", "kera", "musang"]
// angka bisa di konversi ke alphabet tanpa bantuan lain

// slice
var angka = [0, 1, 2, 3]
var irisan1 = angka.slice(1,3) 
console.log(irisan1) //[1, 2]
var irisan2 = angka.slice(0,2)
console.log(irisan2) //[0, 1];

var angka = [0, 1, 2, 3]
var irisan3 = angka.slice(2)
console.log(irisan3) // [2, 3];

var angka = [0, 1, 2, 3]
var salinAngka = angka.slice()
console.log(salinAngka) // [0, 1, 2, 3];

//coba slice
var a = [0,1,2,3,4,5,6]
var cobaslice1 = a.slice(0,3,5)
console.log(cobaslice1)
// catatan: * konsep irisan, angka pertama hasil = alat iris ke-1 dan 
//           angka terakhir hasil = angka alat iris ke-2 (dikurangi) 1
//          ** slice hanya bisa menggunakan alat slice = 2 angka

// splice
//rumus ==> array.splice([IndexMulai], [JumlahNilaiYangDihapus], [NilaiYangDitambahkan1], [NilaiYangDitambahkan2], ...);
var fruits = [ "banana", "orange", "grape"]
fruits.splice(1, 0, "watermelon") 
console.log(fruits) // [ "banana", "watermelon", "orange", "grape"]

var fruits = [ "banana", "orange", "grape"]
fruits.splice(0, 2)
console.log(fruits) // ["grape"]

// split
// gunanya memecah sebuah string sehingga menjadi sebuah array
// rumus ==> array.split(nilai penambahan)
var biodata = "name:john,doe" 
var baru = biodata.split(":")
console.log(baru) // [ "name", "john,doe"]

// join
// gunanya mengubah sebuah array menjadi string
var title = ["my", "first", "experience", "as", "programmer"] 
var slug = title.join(" ")
console.log(slug) // "my-first-experience-as-programmer"

/// FUNCTION
// rumus function
//function nama_function(parameter 1, parameter 2, ...) {
//  [Isi dari function berupa tindakan]
//  return [expression];
//}

function tampilkan() {
  console.log("halo!");
}
 
tampilkan(); // halo!

//

function munculkanAngkaDua() {
  return 2
}
 
var tampung = munculkanAngkaDua();
console.log(tampung); // 2

//

function kalikanDua(angka) {
  return angka * 2
}
 
var tampung = kalikanDua(2);
console.log(tampung); // 4, karna 2 dikalikan 2

//

function tampilkanAngka(angkaPertama, angkaKedua) {
  return angkaPertama + angkaKedua
}
 
console.log(tampilkanAngka(5, 3));

//

function tampilkanAngka(angka = 1) {
  return angka
}
 
console.log(tampilkanAngka(5)); // 5, sesuai dengan nilai parameter yang dikirim
console.log(tampilkanAngka()); // 1, karena default dari parameter adalah 1

//

var fungsiPerkalian = function(angkaPertama, angkaKedua) {   
   return angkaPertama * angkaKedua 
}
console.log(fungsiPerkalian(2, 4)); //8, karna perkalian 4 dan 2

/// OBJECT
// rumus object
//var object = {
//    [key]: [value]
// }
var personArr = ["John", "Doe", "male", 27]
var personObj = {
    firstName : "John",
    lastName: "Doe",
    gender: "male",
    age: 27
}
console.log(personArr[0]); // John
console.log(personObj.firstName); // John

//

var car2 = {}
// meng-assign key:value dari object car2
car2.brand = "Lamborghini"
car2.brand = "Sports Car"
car2.price = 100000000
car2["horse power"] = 730

//

var motorcycle1 = {    
    brand: "Handa",
    type: "CUB",
    price: 1000
}
console.log(motorcycle1.brand) // "Handa"
console.log(motorcycle1.type) // "CUB"
console.log(motorcycle1["price"])
// bisa menggunakan "variable.variabel dalam kurawal"
//              atau "variable["variabel dalam kurawal"]
// catatan *Hal ini karena jika nama key dari Object 
// lebih dari satu kata atau dipisah dengan spasi kita bisa deklarasikan 
// dengan memberikan tanda petik ("")

//

var array = [ 1, 2, 3 ] 
console.log(typeof array) // object

/// ARRAY OF OBJECT
// menggunakan 3 method ini yaitu forEach(), map() dan filter()

// foreach()
// foreach method untuk array berfungsi untuk perulangan data dari array

var mobil = [{merk: "BMW", warna: "merah", tipe: "sedan"}, {merk: "toyota", warna: "hitam", tipe: "box"}, {merk: "audi", warna: "biru", tipe: "sedan"}]
mobil.forEach(function(item){
   console.log("warna : " + item.warna);
})

// map
// map method untuk array berfungsi untuk membuat array baru

var arrayWarna = mobil.map(function(item){
   return item.warna
})

console.log(arrayWarna);

// filter
// filter method untuk array berfungsi untuk memnyaring data yang diinginkan

var arrayMobilFilter = mobil.filter(function(item){
   return item.tipe != "sedan";
})

console.log(arrayMobilFilter);

