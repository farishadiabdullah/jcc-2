// pakai javascript biasa

function luaspp(panjang,lebar){
    return panjang * lebar
  }
  
  function kelilingpp (panjang, lebar) {
    return ( 2 * panjang ) + (2 * lebar )
  }
  
  var panjang = 5
  var lebar = 3
  
  console.log(kelilingpp(panjang,lebar),luaspp(panjang,lebar));
  
  // pakai cs6
  
  const luasppcs6 = (panjang, lebar) => {
     return `${panjang * lebar}`;
  }
  
  const kelilingppcs6 = (panjang, lebar) => {
     return `${(2 * panjang) + (2 * lebar)}`;
  }
  
  var panjang = 5
  var lebar = 3
  
  console.log(kelilingppcs6(panjang,lebar),luasppcs6(panjang,lebar));


  // soal 2

  // const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }
 
// //Driver Code 
// newFunction("William", "Imoh").fullName()

// const newFunction = (firstName, lastName,) => {
//     return console.log(`${firstName} ${lastName}`)
//   }
   
// //Driver Code 
// newFunction("William", "Imoh")

// jawaban 2 yang kemungkinan bener

const newFunction = (firstName, lastName) => {
    return {
      fullName: function () {
        console.log(`${firstName} ${lastName}`)
      }
    } 
  }
  
//Driver Code 
newFunction("William", "Imoh").fullName()

// soal 3

  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  
  const {firstName, lastName, address, hobby} = newObject
  
  console.log(firstName, lastName, address, hobby);


// Soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]

//Driver Code
console.log(combined);

// soal 5

const planet = "earth" 
const view = "glass"

const gabungan = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`

console.log(gabungan);