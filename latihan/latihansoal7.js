const posts = [
  {
    title: "Post one",
    body: "This is post one"
  },
  {
    title: "Post two",
    body: "This is post two"
  }
]
const newPost = {
  title: "Post three",
  body: "This is post three"
}


const createPost = (post, callback) => {
  setTimeout(() => {
    posts.push(post)
    callback()
  }, 2000)
}


const getPosts = () => {
  setTimeout(() => {
    posts.forEach(post => {
      console.log(post)
    })
  }, 1000)
}

//   createPost(newPost, getPosts)
//   getPosts()

// soal no 7

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 }
]

function readBooks(time, book, callback) {
  console.log(`saya membaca ${book.name}`)
  setTimeout(function () {
    let sisaWaktu = 0
    if (time >= book.timeSpent) {
      sisaWaktu = time - book.timeSpent
      console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
      callback(sisaWaktu) //menjalankan function callback
    } else {
      console.log('waktu saya habis')
      callback(time)
    }
  }, book.timeSpent)
}

// const panggil = () => {
//     setTimeout(() => {
//         readBooks.forEach( => {
//             console.log()
//         });
//     }, 10000)
// }

readBooks("LOTR",);


// users: [
//   {
//     'name': 'Muhammad Iqbal Mubarok'
//   },
//   {
//     'name': 'Ruby Purwanti'
//   },
//   {
//     'name': 'Faqih Muhammad'
//   }
// ],

// let obj = {
//     a: "makan",
//     b: "tidur",
//     c: "ngoding",
//     d: "minum"
//    }

// const array = Object.entries(obj);

// const objFromArray = Object.fromEntries(array);

// console.log(objFromArray);

// const presensi = [
//     {
//       "NPM": "16753001"
//     },
//     {
//       "NPM": "16753002"
//     },
//     {
//       "NPM": "16753005"
//     }
//   ]

//   const newPresensi = presensi.map(data=>{
//     return data.NPM})

//   console.log(presensi)

const users = [
  {
    'name': 'Muhammad Iqbal Mubarok'
  },
  {
    'name': 'Ruby Purwanti'
  },
  {
    'name': 'Faqih Muhammad'
  },
]
const newuser = users.map(newuser => {
  return newuser.name
})

console.log(newuser)